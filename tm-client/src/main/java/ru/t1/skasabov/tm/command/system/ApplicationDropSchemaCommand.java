package ru.t1.skasabov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.request.ApplicationDropSchemaRequest;
import ru.t1.skasabov.tm.util.TerminalUtil;

public final class ApplicationDropSchemaCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "drop-schema";

    @NotNull
    private static final String DESCRIPTION = "Drop database schema.";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DROP SCHEMA]");
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final ApplicationDropSchemaRequest request = new ApplicationDropSchemaRequest(password);
        getSystemEndpoint().dropSchema(request);
    }

}
