package ru.t1.skasabov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.UserDTO;

public interface IUserDTORepository extends IDTORepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    @NotNull
    Boolean isEmailExist(@NotNull String email);

}
