package ru.t1.skasabov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.dto.IDTOService;
import ru.t1.skasabov.tm.dto.model.AbstractModelDTO;

public abstract class AbstractDTOService<M extends AbstractModelDTO> implements IDTOService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    protected AbstractDTOService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
