package ru.t1.skasabov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ApplicationDropSchemaRequest extends AbstractRequest {

    @Nullable
    private String password;

    public ApplicationDropSchemaRequest(@Nullable final String password) {
        this.password = password;
    }

}
