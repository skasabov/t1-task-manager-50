package ru.t1.skasabov.tm.exception.field;

import ru.t1.skasabov.tm.exception.user.AbstractUserException;

public final class RoleIncorrectException extends AbstractUserException {

    public RoleIncorrectException() {
        super("Error! Role is incorrect...");
    }

}
